from datetime import timedelta, datetime

from dateutil.parser import parse
from sharedmodel.enum import ModificationCurrency


def is_fz223_id(x: str):
    if '-' in x:
        x = x.split('-')[0]
    return x and len(x) == 11 and x.isdigit()


def format_dt(dt: datetime):
    if dt:
        return dt.strftime('%d.%m.%Y')


def to_float(x: str):
    try:
        return float(''.join(x.split()).replace(',', '.'))
    except Exception:
        return


def convert_dt_str_to_timestamp_mls(dt: str):
    if not dt:
        return None
    vals = dt.split()
    l = len(vals)
    result = None
    if l == 1:
        result = parse(vals[0], dayfirst=True)
    elif l == 2:
        result = parse(dt, dayfirst=True) - timedelta(hours=3)
    elif l == 3:
        result = parse('{} {}'.format(vals[0], vals[1]), dayfirst=True) - timedelta(hours=3)
    return int(result.timestamp() * 1000) if result else None

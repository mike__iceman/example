import typing
from itertools import chain

from payne_core.config import conf
from payne_core.models import Notification, Attachment, Customer, Position
from payne_core.mappings import PlacingWayEnum
from payne_core.utils.resolver import dict_resolver
from payne_core.mappings.currency import get_currency_by_full_name
from payne_core.store import get_org_client
from payne_core.utils.dt import current_timestamp_ms

from src.tools import to_float, convert_dt_str_to_timestamp_mls
from src.mappings import get_placing_way, get_status_code


class NotificationBuilder:
    PREF_NOT_DISHONEST = 10

    __data: typing.Dict
    __data_customer: typing.Dict

    def __init__(self, data):
        self.__data = data

    def _extract_data(self, raw_data):
        data = dict()
        if not raw_data:
            return data

        for table in raw_data.find_all('table', attrs={'class': 'details-table'}):
            for tr in table.find_all('tr'):
                key, val = (x for x in tr.find_all('td'))
                link_val = val.find('a')
                if link_val:
                    data[key.text] = dict(
                        text=' '.join(link_val.text.split()),
                        url=link_val['href']
                    )
                else:
                    data[key.text] = ' '.join(val.text.split())

        for table in raw_data.find_all('table', attrs={'class': 'grid-table-editable'}):
            curr_table_name = table['data-orm-table-id']

            curr_data_list = []
            curr_headers = []
            headers_handled = False
            for row in table.find_all('tr'):
                if not headers_handled:
                    headers = [x.text for x in row.find_all('th')]
                    headers_handled = True
                    continue

                # Костыль от криворуких верстал etprf.ru
                if 'Под этап' in headers:
                    name_val = row.find_all('td')
                    data[name_val[0].text] = ' '.join(name_val[1].text.split())
                    continue

                data_row = dict()
                for header, cell in zip(headers, row.find_all('td')):
                    if not header.strip() and not cell.text.strip() or cell.text.strip() == '(нет данных)':
                        continue
                    link_cell = cell.find('a')
                    if link_cell:
                        data_row[header] = dict(
                            text=' '.join(link_cell.text.split()),
                            url=link_cell['href']
                        )
                    else:
                        data_row[header] = ' '.join(cell.text.split())

                if data_row:
                    curr_data_list.append(data_row)

            if curr_table_name not in data:
                data[curr_table_name] = []
            data[curr_table_name].extend(curr_data_list)

        return data

    def build_short(self):
        self.__data = self._extract_data(self.__data)

        n = Notification()
        self.notification_id(n=n)
        self.notification_id_internal(n=n)
        self.notification_number(n=n)
        self.notification_customer_uri(n=n)
        self.notification_is_customer_empty(n=n)
        return n


    def build_full(self, n: Notification, data_customer=None):
        self.__data_customer = self._extract_data(data_customer)

        self.notification_href(n=n)
        self.notification_order_name(n=n)
        self.notification_currency(n=n)
        self.notification_multilot(n=n)
        self.notification_customers(n=n)
        self.notification_region(n=n)
        self.notification_max_price(n=n)
        self.notification_guarantee_app(n=n)
        self.notification_guarantee_contract(n=n)
        self.notification_preferences(n=n)
        self.notification_publication_date_time(n=n)
        self.notification_submission_start_date_time(n=n)
        self.notification_submission_close_date_time(n=n)
        self.notification_scoring_date_time(n=n)
        self.notification_bidding_date_time(n=n)
        self.notification_opening_date_time(n=n)
        self.notification_final_opening_date_time(n=n)
        self.notification_attachments(n=n)
        self.notification_positions(n=n)
        self.notification_status(n=n)
        self.notification_placing_way(n=n)
        self.notification_contact_org_name(n=n)
        self.notification_contact_org_post_address(n=n)
        self.notification_contact_org_fact_address(n=n)
        self.notification_contact_fio(n=n)
        self.notification_contact_phone(n=n)
        self.notification_contact_email(n=n)
        self.notification_contact_fax(n=n)
        self.notification_submission_start_place(n=n)
        self.notification_submission_start_order(n=n)
        self.notification_delivery_place(n=n)
        self.notification_scoring_order(n=n)
        self.notification_bidding_order(n=n)
        self.notification_final_opening_order(n=n)
        return n

    def notification_id(self, n: Notification):
        num = dict_resolver(self.__data, 'Номер извещения')
        l_num = dict_resolver(self.__data, 'Номер лота', default=1)
        if num and l_num:
            n.id = '{}_{}'.format(num, l_num)

    def notification_id_internal(self, n: Notification):
        href = dict_resolver(self.__data, 'Ссылка на опубликованное извещение.url') \
            or dict_resolver(self.__data, 'Извещение.url')
        if href:
            n._id_internal = href.split('/')[-1]

    def notification_number(self, n: Notification):
        n.number = dict_resolver(self.__data, 'Номер извещения')

    def notification_href(self, n: Notification):
        href = dict_resolver(self.__data, 'Ссылка на опубликованное извещение.url') \
            or dict_resolver(self.__data, 'Извещение.url')
        if href:
            n.href = href if 'http://' in href or 'https://' in href else '{}{}'.format(conf.info.url, href)

    def notification_order_name(self, n: Notification):
        n.orderName = dict_resolver(self.__data, 'Наименование заказа')

    def notification_currency(self, n: Notification):
        currency = dict_resolver(self.__data, 'Валюта') or 'Российский рубль'
        n.currency = get_currency_by_full_name(currency).name.lower()

    def notification_multilot(self, n: Notification):
        multilot = dict_resolver(self.__data, 'Многолотовое извещение')
        if multilot:
            n.multilot = multilot == 'Да'
            return

        n.multilot = dict_resolver(self.__data, 'Номер лота') is not None

    def notification_is_customer_empty(self, n: Notification):
        n.is_customer_empty = dict_resolver(self.__data, 'ИНН') is None

    def notification_customer_uri(self, n: Notification):
        n.customer_uri = dict_resolver(self.__data, 'Наименование организации.url')

    def notification_customers(self, n: Notification):
        org_client = get_org_client()

        for req in dict_resolver(self.__data, 'CustomerRequirements', to_list=True) or [{},]:
            name = dict_resolver(req, 'Наименование организации') or \
                   dict_resolver(self.__data, 'Наименование организации, размещающей заказ')
            inn = dict_resolver(self.__data, 'ИНН') or dict_resolver(self.__data_customer, 'ИНН')
            kpp = dict_resolver(self.__data, 'КПП') or dict_resolver(self.__data_customer, 'КПП')

            customer = org_client.get(name=name, inn=inn, kpp=kpp)
            if customer['guid']:
                c = Customer()
                c.guid = customer['guid']
                c.name = customer['name']
                c.region = customer['region']
                c.inn = inn
                c.kpp = kpp
                c.max_price = to_float(dict_resolver(req, 'Начальная цена договора'))
                c.guarantee_app = to_float(dict_resolver(
                    self.__data, 'Размер обеспечения(резервирования оплаты) заявки на участие, в рублях'))
                c.guarantee_contract = to_float(dict_resolver(req, 'Размер обеспечения договора'))
                duplicate = False
                for other_customer in n.customers:
                    if customer.guid and customer.guid == other_customer.guid:
                        duplicate = True
                        break

                if not duplicate:
                    n.customers.add(c)

    def notification_region(self, n: Notification):
        for c in n.customers:
            if c.region:
                n.region = c.region
                break

    def notification_max_price(self, n: Notification):
        max_price = sum((c.max_price for c in n.customers if c.max_price))
        if not max_price:
            max_price = to_float(dict_resolver(self.__data, 'Начальная цена'))

        if max_price:
            n.maxPrice = max_price

    def notification_guarantee_app(self, n: Notification):
        guarantee_app = sum((c.guarantee_app for c in n.customers if c.guarantee_app))
        if not guarantee_app:
            guarantee_app = to_float(dict_resolver(self.__data, 'Размер обеспечения заявки'))

        if guarantee_app:
            n.guaranteeApp = guarantee_app

    def notification_guarantee_contract(self, n: Notification):
        guarantee_contract = sum((c.guarantee_contract for c in n.customers if c.guarantee_contract))
        if not guarantee_contract:
            guarantee_contract = to_float(dict_resolver(self.__data, 'Размер обеспечения контракта'))

        if guarantee_contract:
            n.guaranteeContract = guarantee_contract

    def notification_preferences(self, n: Notification):
        preferences = []
        is_not_dishonest = dict_resolver(
            self.__data, 'Требование к отсутствию участника в реестре недобросовестных поставщиков')

        if is_not_dishonest and is_not_dishonest == 'Да':
            preferences.append(self.PREF_NOT_DISHONEST)

        if preferences:
            n.preference = preferences

    def notification_publication_date_time(self, n: Notification):
        n.publicationDateTime = convert_dt_str_to_timestamp_mls(dict_resolver(
            self.__data, 'Планируемая дата публикации извещения')) or current_timestamp_ms()

    def notification_submission_start_date_time(self, n: Notification):
        sub_start_date_time = dict_resolver(self.__data, 'Дата и время начала подачи заявок')
        if sub_start_date_time:
            n.submissionStartDateTime = convert_dt_str_to_timestamp_mls(sub_start_date_time.split('\\')[0])

    def notification_submission_close_date_time(self, n: Notification):
        sub_close_date_time = dict_resolver(self.__data, 'Окончание срока подачи заявок')
        if sub_close_date_time:
            n.submissionCloseDateTime = convert_dt_str_to_timestamp_mls(sub_close_date_time.split('\\')[0])

    def notification_scoring_date_time(self, n: Notification):
        scoring_date_time = dict_resolver(self.__data, 'Дата и время рассмотрения заявок')
        if scoring_date_time:
            n.scoringDateTime = convert_dt_str_to_timestamp_mls(scoring_date_time)

    def notification_bidding_date_time(self, n: Notification):
        bidding_date_time = dict_resolver(self.__data, 'Дата и время проведения торгов')
        if bidding_date_time:
            n.biddingDateTime = convert_dt_str_to_timestamp_mls(bidding_date_time)

    def notification_opening_date_time(self, n: Notification):
        opening_date_time = dict_resolver(self.__data, 'Дата и время вскрытия конвертов с заявками')
        n.opening_datetime = convert_dt_str_to_timestamp_mls(opening_date_time)

    def notification_final_opening_date_time(self, n: Notification):
        final_opening_date_time = dict_resolver(self.__data, 'Дата и время подведения итогов')
        n.final_opening_datetime = convert_dt_str_to_timestamp_mls(final_opening_date_time)

    def notification_attachments(self, n: Notification):
        attachments = []
        for at in chain(
            dict_resolver(self.__data, 'DocumentMetas', to_list=True),
            dict_resolver(self.__data, 'OtherFiles', to_list=True)
        ):
            attachment = Attachment()
            attachment.realName = dict_resolver(at, 'Наименование файла.text')
            attachment.displayName = dict_resolver(at, 'Наименование документа')
            attachment.href = dict_resolver(at, 'Наименование файла.url')

            attachment.publicationDateTime = convert_dt_str_to_timestamp_mls(
                dict_resolver(at, 'Дата загрузки')) or n.publicationDateTime
            if attachment.href:
                attachments.append(attachment)

        if attachments:
            n.attachments = attachments

    def notification_positions(self, n: Notification):
        positions = []
        for pos in dict_resolver(self.__data, 'LotItems', to_list=True):
            position = Position()
            position.name = dict_resolver(pos, 'ОКПД2')
            position.quantity = dict_resolver(pos, 'Количество')
            position.unit = dict_resolver(pos, 'Единица измерения')

            position.classification_code = dict_resolver(pos, 'ОКПД2 код')
            position.classification_name = dict_resolver(pos, 'ОКПД2')
            position.classification_type = 'ОКПД2'
            positions.append(position)
        
        if positions:
            n.positions = positions

    def notification_placing_way(self, n: Notification):
        placing_way = dict_resolver(self.__data, 'Способ закупки')
        if placing_way:
            n.placingWay = get_placing_way(placing_way)

        if not n.placingWay:
            n.placingWay = PlacingWayEnum.Undefined.value

    def notification_contact_org_name(self, n: Notification):
        n.contact_org_name = dict_resolver(self.__data, 'Наименование организации, размещающей заказ')

    def notification_contact_org_post_address(self, n: Notification):
        n.contact_org_post_address = dict_resolver(self.__data, 'Почтовый адрес организации')

    def notification_contact_org_fact_address(self, n: Notification):
        n.contact_org_fact_address = dict_resolver(self.__data, 'Адрес местонахождения организации')

    def notification_contact_fio(self, n: Notification):
        n.contact_fio = dict_resolver(self.__data, 'Контактное лицо')

    def notification_contact_phone(self, n: Notification):
        n.contact_fio = dict_resolver(self.__data, 'Телефон контактного лица')

    def notification_contact_email(self, n: Notification):
        n.contact_email = dict_resolver(self.__data, 'e-mail адрес контактного лица')

    def notification_contact_fax(self, n: Notification):
        n.contact_fax = dict_resolver(self.__data, 'Факс контактного лица')

    def notification_delivery_place(self, n: Notification):
        n.delivery_place = dict_resolver(self.__data, 'Место поставки, выполнения работ, оказания услуг')

    def notification_submission_start_place(self, n: Notification):
        n.submission_start_place = dict_resolver(self.__data,  'Место подачи заявок')

    def notification_submission_start_order(self, n: Notification):
        n.submission_start_order = dict_resolver(self.__data, 'Порядок подачи заявок')

    def notification_scoring_order(self, n: Notification):
        n.scoring_order = dict_resolver(self.__data, 'Порядок рассмотрения заявок')

    def notification_bidding_order(self, n: Notification):
        n.bidding_order = dict_resolver(self.__data, 'Порядок проведения аукциона')

    def notification_final_opening_order(self, n: Notification):
        n.final_opening_order = dict_resolver(self.__data, 'Порядок подведения итогов')

    def notification_status(self, n: Notification):
        status = dict_resolver(self.__data, 'Статус закупки')
        if status:
            n.status = get_status_code(status)
            return

        status = dict_resolver(self.__data, 'Состояние извещения') \
            or dict_resolver(self.__data, 'Статус публикации лота извещения')
        n.status = get_status_code(status, n.submissionCloseDateTime)
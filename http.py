import logging
from datetime import datetime, timedelta

from bs4 import BeautifulSoup, Tag
from payne_core.utils.dt import parse
from payne_core.config import conf
from payne_core import requests
from payne_core.requests import Session
from payne_core.utils.retrier import retry

from src.tools import is_fz223_id, to_float, convert_dt_str_to_timestamp_mls, format_dt

BASE_URL = 'http://etprf.ru'

logger = logging.getLogger('http')

class ListItem:
    type: str
    platform_name: str
    number: str
    order_name: str
    max_price: str
    guarantee_app: float
    guarantee_contract: float
    organizer: str
    pub_date: int
    sub_close_date_time: int
    status: str
    href: str

    def __init__(self):
        self.type = None
        self.platform_name = None
        self.number = None
        self.order_name = None
        self.max_price = None
        self.guarantee_app = None
        self.guarantee_contract = None
        self.organizer = None
        self.pub_date = None
        self.sub_close_date_time = None
        self.status = None
        self.href = None

    @staticmethod
    def parse(data: Tag):
        item = ListItem()
        item.number = data.contents[2].text
        item.type = data.contents[0].text
        item.platform_name = data.contents[1].text
        item.order_name = data.contents[3].text
        item.max_price = to_float(data.contents[4].text)
        item.guarantee_app = to_float(data.contents[5].text)
        item.guarantee_contract = to_float(data.contents[5].text)
        item.organizer = data.contents[7].text
        item.pub_date = convert_dt_str_to_timestamp_mls(data.contents[8].text)
        item.sub_close_date_time = convert_dt_str_to_timestamp_mls(data.contents[9].text)
        item.status = data.contents[10].text
        item.href = '{}{}'.format(BASE_URL, data.contents[11].find('a')['href'])
        return item


class Http:
    HEADERS = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

    def __init__(self):
        self.wrong_states = (
            'Вход заказчиком',
            '403 - запрещено. Доступ запрещен.',
            '404 - файл или каталог не найден.',
            '404 - File or directory not found.',
            'Тип входа:'
        )

        self.session = Session()

    @retry()
    def get_tenders(self, page: int = 1, pub_date_from=None, pub_date_to=None,
                    sub_close_date_from=None, sub_close_date_to=None):

        body = {
            '_orm_ClientType': 'Browser',
            '_orm_PageID': '522B5C01862DB8C0',
            'AutoSaveField': None,
            'Filter.FastFilter': None,
            'SortColumn522B5C01862DB8C0': 'PublicationDateTime',
            'SortColumnDesc522B5C01862DB8C0': 1,
            'Filter.NotificationNumber': None,
            'Filter.OrderName': None,
            'Filter.Placer': None,
            'Filter.Customer': None,
            'Filter.nsiOkpd2': None,
            'Filter.StartPriceFrom': None,
            'Filter.StartPriceTo': None,
            'Filter.LotStartPriceFrom': None,
            'Filter.LotStartPriceTo': None,
            'Filter.ContractStartPriceFrom': None,
            'Filter.ContractStartPriceTo': None,
            'Filter.PublicationDateTimeFrom': format_dt(pub_date_from),
            'Filter.PublicationDateTimeTo': format_dt(pub_date_to),
            'Filter.SubmissionOpenDateTimeFrom': None,
            'Filter.SubmissionOpenDateTimeTo': None,
            'Filter.SubmissionCloseDateTimeFrom': format_dt(sub_close_date_from),
            'Filter.SubmissionCloseDateTimeTo': format_dt(sub_close_date_to),
            'Filter.AuctionTimeFrom': None,
            'Filter.AuctionTimeTo': None,
            'Filter.ItForSMB': None,
            '_orm_SerializableTableKey': '77018CA04FC7D0E3',
            'PageSize522B5C01862DB8C0': 1000,
            'PageNumberView1522B5C01862DB8C0': page,
            'PageNumber522B5C01862DB8C0': page
        }
        params = {
            'Sources': 1,  # ЭТП,
            'ProcedureType': '2,3',  # 223-ФЗ, Коммерческие
            'IsPartialView': 1,
            'IsTableContentOnlyRequest': 1
        }
        if pub_date_from:
            params['PublicationDateTimeFrom'] = format_dt(pub_date_from)

        if pub_date_to:
            params['PublicationDateTimeTo'] = format_dt(pub_date_to)

        if sub_close_date_from:
            params['SubmissionCloseDateTimeFrom'] = format_dt(sub_close_date_from)

        if sub_close_date_to:
            params['SubmissionCloseDateTimeTo'] = format_dt(sub_close_date_to)

        res = self.session.post(url='http://etprf.ru/NotificationCR',
                                params=params, data=body, headers=self.HEADERS, timeout=2000, proxies=conf.proxy)
        tenders = []
        if res.status_code == 200:
            dom = BeautifulSoup(res.content, 'lxml')
            for tr in dom.find_all('tr'):
                # только у строк с данными есть ид
                # убираем шапку таблицы, и пустую строку если нет результатов
                if not tr.has_attr('id'):
                    continue
                item = ListItem.parse(tr)
                # проверям номер на подобие 223-ФЗ
                # данные не должны содержать тендеры по 223-ФЗ
                if is_fz223_id(item.number):
                    continue
                tenders.append(item)
        return tenders

    @retry()
    def list_gen(self, pub_date_from=None, pub_date_to=None, sub_close_date_from=None, sub_close_date_to=None):
        page = 1
        while page:
            tenders = self.get_tenders(page=page,
                                       pub_date_from=pub_date_from,
                                       pub_date_to=pub_date_to,
                                       sub_close_date_from=sub_close_date_from,
                                       sub_close_date_to=sub_close_date_to)
            if not tenders:
                break
            for tender in tenders:
                yield tender
            page += 1

    @retry(attempts=20, delay=2000)
    def get_single(self, url: str):
        res = self.session.get(url=url, headers=self.HEADERS, timeout=20, proxies=conf.proxy)
        logger.info(res.url)
        tag = BeautifulSoup(res.content, 'lxml')
        if not any(x in tag.text for x in self.wrong_states):
            return tag

    @retry(attempts=20, delay=2000)
    def get_customer(self, uri: str):
        res = self.session.get(f'{BASE_URL}{uri}', headers=self.HEADERS, timeout=20, proxies=conf.proxy)
        tag = BeautifulSoup(res.content, 'lxml')
        if not any(x in tag.text for x in self.wrong_states):
            return tag

from datetime import datetime, timedelta

from payne_core.api.base import CollectorBase

from src.http import Http
from src.builder import NotificationBuilder
from src.mappings import get_status_code


class Base(CollectorBase):
    def collect_multiple(self, date_from: str = None, date_to: str = None):
        # площадка не позволяет фильтровать по дате обновления,
        # поэтому фильтруем по:
        # 1) дате публикации - today-1d -> today (собираем актуальные)
        # 2) дате окончания подачи заявок today -> today+30d (собираем измененные)
        http = Http()

        # актуальные
        pub_date_from = date_from or datetime.now() - timedelta(days=1)
        pub_date_to = date_to or datetime.now()
        self.logger.info('processing actual tenders pub_date_from=[{}] pub_date_to=[{}]'.format(
            pub_date_from, pub_date_to))
        for item in http.list_gen(pub_date_from=pub_date_from, pub_date_to=pub_date_to):
            if self._is_exists(item=item):
                continue
            self.logger.info('processing tender tender_id=[{}]'.format(item.number))
            self.__process_single(url=item.href)

            # если хотя бы одна из дат есть, то это пересбор  а значит следущую часть выполнять не нужно
        if date_from or date_to:
            return

        # обновления
        sub_close_date_from = datetime.now() - timedelta(days=1)
        sub_close_date_to = datetime.now() + timedelta(days=30)
        self.logger.info('processing update tenders sub_close_date_from=[{}] sub_close_date_to=[{}]'.format(
            sub_close_date_from, sub_close_date_to))
        for item in http.list_gen(sub_close_date_from=sub_close_date_from, sub_close_date_to=sub_close_date_to):
            if self._is_exists(item=item):
                continue
            self.logger.info('processing tender tender_id=[{}]'.format(item.number))
            self.__process_single(url=item.href)

    def collect_single(self, url: str):
        self.__process_single(url=url)

    def _is_exists(self, item):
        num = item.href.split('/')[-1]
        recent = self.model_store.get_tender(id_=num)
        is_exists = recent and recent['status'] == get_status_code(item.status, item.sub_close_date_time)
        if is_exists:
            self.logger.info('tender already exists tender_id=[{}]'.format(item.number))
        return is_exists

    def __process_single(self, url: str):
        http = Http()
        data = http.get_single(url=url)
        if not data:
            self.logger.info(f'no access {url}')
            return

        builder = NotificationBuilder(data=data)
        n_short = builder.build_short()
        data_customer = http.get_customer(uri=n_short.customer_uri) if n_short.is_customer_empty else None
        notification = builder.build_full(n=n_short, data_customer=data_customer)

        notification_dict = notification.as_dict()
        self.api.publish_tender(model=notification_dict)
        self.model_store.upsert_tender(id_=notification._id_internal, status=notification.status)

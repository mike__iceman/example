from payne_core.mappings import PlacingWayEnum, StatusCodeEnum
from payne_core.store import get_sentry_client
from payne_core.store.sentry.exceptions import PlacingWayNotFound, StatusNotFound
from payne_core.utils.dt import current_timestamp_ms


# обязательно от более узкого понятия к широкому
placing_ways = {
    'Квалификационный отбор для серии закупок в электронной форме': PlacingWayEnum.Undefined,
    'Запрос предложений в электронной форме': PlacingWayEnum.RequestProposalsElectronic,
    'Запрос предложений коммерческий отдел': PlacingWayEnum.RequestProposal,
    'Запрос котировок в электронной форме': PlacingWayEnum.RequestQuotationElectronic,
    'Запрос цен в электронной форме': PlacingWayEnum.RequestQuotationElectronic,
    'Сбор коммерческих предложений': PlacingWayEnum.Undefined,
    'Редукцион в электронной форме': PlacingWayEnum.Reduction,
    'Конкурс в электронной форме': PlacingWayEnum.CompetitionLimitedElectronic,
    'Тендер в электронной форме': PlacingWayEnum.Undefined,
    'Запрос предложений': PlacingWayEnum.RequestProposal,
    'Запрос котировок': PlacingWayEnum.RequestQuotation,
    'Запрос котировок': PlacingWayEnum.RequestQuotation,
    'Открытый конкурс': PlacingWayEnum.CompetitionOpen,
    'Мониторинг цен': PlacingWayEnum.Undefined,
    'Запрос цен': PlacingWayEnum.RequestQuotation,
    'Редукцион': PlacingWayEnum.Reduction,
    'Мониторинг': PlacingWayEnum.Undefined,
    'Тендер': PlacingWayEnum.Undefined,
    'Предквалификационный отбор': PlacingWayEnum.Undefined,
    'Закупка в электронном виде': PlacingWayEnum.Undefined,
    'РСХБ_Открытый аукцион в электронной форме': PlacingWayEnum.AuctionOpenElectronic
}

states = {
    'Подача заявок на участие': StatusCodeEnum.Active.value,
    'Подача заявок завершена': StatusCodeEnum.Closed.value,
    'Отменен': StatusCodeEnum.Cancel.value,
    'Не состоялся': StatusCodeEnum.Abandoned.value,
    'Закрыт': StatusCodeEnum.Closed.value
}


def get_placing_way(name: str):
    for k, v in placing_ways.items():
        if k.lower() in name.lower():
            return v.value
    get_sentry_client().capture_exception(PlacingWayNotFound(name))


def get_status_code(name: str, sub_close_date: int = None):
    if name:
        for k, v in states.items():
            if name.lower() in k.lower():
                return v

    if sub_close_date:
        return (StatusCodeEnum.Cancel if name == 'Отменено' else StatusCodeEnum.Active if sub_close_date > current_timestamp_ms()
                else StatusCodeEnum.Closed).value
    get_sentry_client().capture_exception(StatusNotFound(name))
